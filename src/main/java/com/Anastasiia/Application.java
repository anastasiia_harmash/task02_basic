/**
 * This application print odd numbers from some interval
 * from begin of interval to the end.
 * And print even numbers from end of interval
 * to the begin
 */

package com.Anastasiia;

import java.util.Scanner;

/**
 * @author Anastasiia Harmash
 * @version 06 nov 2019
 */

public class Application {
    /** Main class
     *
     * @param args
     */
    public static void main(String[] args) {
/** User enter the interval (begin and end of interval)
 *
 */

        Scanner in = new Scanner(System.in);
        System.out.println("Write the begin of interval: ");
        int beginInterval = in.nextInt();
        System.out.println("Write the end of interval: ");
        int endInterval = in.nextInt();
        in.close();

        int d; //counter
        /** From begin to the end of interval the numbers are
         * being checked for unpaired and displayed.
         */

        for (d = beginInterval; d <= endInterval; d++) {
            if (d % 2 != 0) {
                System.out.print(d + " ");
            }
        }

        System.out.println(); // New line
/** From end to the begin of interval
 * the numbers are being checked for pairing and displayed.
 */
        for (d = endInterval; d >= beginInterval; d--) {
            if (d % 2 == 0) {
                System.out.print(d + " ");
            }
        }


    }
}

package com.Anastasiia;

import java.util.Scanner;

/**
 * @author Anastasiia Harmash
 * @version 06 nov 2019
 */

public class Application2 {

        /** Main class
         *
         * @param args
         */
    public static void main(String[] args) {

        /* User enter the size of set (N) */

        System.out.println("Enter the size of set:");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.close();

        /* Check the correct size of set */

        if (n < 2) {
            System.out.println("Minimum size: 2!");
            System.exit(0);
        }
        int i; //counter

        /* Create new array Fibonacci numbers (size = N) */

        int[] fibonacci = new int[n];

        fibonacci[0] = 1; // 0 element of array must be 1
        fibonacci[1] = 1; // 1 element of array must be 1
        i = 2; // begin count from 2 element of set

        /* Create the Fibonacci array */

        while (i < fibonacci.length) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            i++;
        }
        /*Print the Fibonacci array*/

        for (i = 0; i < fibonacci.length; i++) {
            System.out.print(fibonacci[i] + " ");
        }

        int f1; //F1 will be the biggest odd number
        f1 = 0;
        int f2; //F2 will be the biggest even number
        f2 = 0;
        int numOdd = 0; //amount of odd numbers
        int numEven = 0; //amount of even numbers

        for (i = 0; fibonacci.length > i; i++) {
            if (fibonacci[i] % 2 != 0) {
                /* Count amount of odd numbers */
                numOdd = numOdd + 1;
                if (fibonacci[i] > f1) {
                    /* Find biggest odd number */
                    f1 = fibonacci[i];
                }
            } else {
                /* If the number is paired */
                /* Count amount of even numbers */
                numEven = numEven + 1;
                if (fibonacci[i] > f2) {
                    /* Find biggest even number */
                    f2 = fibonacci[i];
                }
            }

        }
        double percentOdd;
        double percentEven;

        percentOdd = (numOdd * 100) / fibonacci.length;
        percentEven = (numEven * 100) / fibonacci.length;

        System.out.println("The biggest odd number is: " + f1);
        System.out.println("The biggest even number is: " + f2);
        System.out.println("percentage of Odd numbers is: " + percentOdd + "%");
        System.out.println("percentage of Even numbers is: " + percentEven + "%");
    }


}



package com.Anastasiia;

import java.util.Scanner;

/**
 * @author Anastasiia Harmash
 * @version 06 nov 2019
 */

public class Application1 {
    /** Main class
     *
     * @param args
     */
    public static void main(String[] args) {

        int d; // counter
        int sumOdd = 0; // sum of odd numbers
        int sumEven = 0; // sum of even numbers

        /** User enter the interval (begin and end of interval)
         *
         */

        Scanner in = new Scanner(System.in);
        System.out.println("Write the begin of interval: ");
        int beginInterval = in.nextInt();
        System.out.println("Write the end of interval: ");
        int endInterval = in.nextInt();
        in.close();

        /** From begin to the end of interval
         *  the numbers are
         * being checked for unpaired and
         their sum is calculated.
         */

        for (d = beginInterval; d <= endInterval; d++) {
            if (d % 2 != 0) {
                sumOdd = sumOdd + d;
            }
        }
        System.out.print("Sum of odd numbers = " + sumOdd); //Print sum of odd numbers

        System.out.println(); // New line

        /** From end to the begin of interval
         * the numbers are
         * being checked for pairing and
         their sum is calculated.
         */
        for (d = endInterval; d >= beginInterval; d--) {
            if (d % 2 == 0) {
                sumEven = sumEven + d;
            }
        }
        System.out.print("Sum of even numbers = " + sumEven); //Print sum of even numbers

    }

}
